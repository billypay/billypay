'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.addColumn(models.User.tableName, 'ip', { type: Sequelize.STRING });
    queryInterface.addColumn(models.Account.tableName, 'ip', { type: Sequelize.STRING });
  },

  down: function down(queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.removeColumn(models.User.tableName, 'ip');
    queryInterface.removeColumn(models.Account.tableName, 'ip');
  }
};
