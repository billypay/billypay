'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.addColumn(models.Project.tableName, 'estimatedTime', { type: Sequelize.FLOAT, defaultValue: 0 });
    queryInterface.addColumn(models.ProjectCategory.tableName, 'estimatedTime', { type: Sequelize.FLOAT, defaultValue: 0 });
  },

  down: function down(queryInterface
  ) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.removeColumn(models.Project.tableName, 'estimatedTime');
    queryInterface.removeColumn(models.ProjectCategory.tableName, 'estimatedTime');
  }
};
