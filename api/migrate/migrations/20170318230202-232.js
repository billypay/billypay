const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */

    // queryInterface.addColumn(models.Invitation.tableName, 'role', { type: Sequelize.INTEGER, defaultValue: 100 });
    queryInterface.addColumn(models.Account.tableName, 'disableNote', { type: Sequelize.BOOLEAN, defaultValue: false });
    queryInterface.addColumn(models.Account.tableName, '_tags', { type: Sequelize.ARRAY(Sequelize.STRING) });
  },
  down: function down(queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    // queryInterface.removeColumn(models.Invitation.tableName, 'role');
    queryInterface.removeColumn(models.Account.tableName, 'disableNote');
    queryInterface.removeColumn(models.Account.tableName, '_tags');
  }
};
