'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.addColumn(models.Account.tableName, 'number', { type: Sequelize.INTEGER });
    queryInterface.addColumn(models.AccountBillInvoice.tableName, 'number', { type: Sequelize.INTEGER });
  },

  down: function down(queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.removeColumn(models.Account.tableName, 'number');
    queryInterface.removeColumn(models.AccountBillInvoice.tableName, 'number');
  }
};
