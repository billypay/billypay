'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: async () => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    const users = await models.User.findAll();

    for (let u = 0; u < users.length; u++) {
      const user = users[u];
      await user.updateAttributes({ accepted: true });
    }
  }
};
