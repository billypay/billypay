'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: async () => {
    // Get all accounts
    const accountsBill = await models.AccountBill.findAll();

    for (let a = 0; a < accountsBill.length; a++) {
      const accountBill = accountsBill[a];
      await accountBill.updateAttributes({ trial: true });
    }
  }
};
