'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.BOOLEAN });
    */
    queryInterface.addColumn(models.User.tableName, 'tour_hide', { type: Sequelize.BOOLEAN, defaultValue: false });
  },

  down: function down(queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.addColumn(models.User.tableName, 'tour_hide');
  }
};
