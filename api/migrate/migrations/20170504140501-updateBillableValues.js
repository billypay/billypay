'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: async () => {
    // Get all accounts
    const accounts = await models.Account.findAll();

    for (let a = 0; a < accounts.length; a++) {
      const account = accounts[a];
      // Get all datas
      // Update account
      await account.updateAttributes({ billableRate: ((account.billableRate || 0) * account.totalHourInDay) });


      const customers = await models.Customer.findAll({ where: { accountId: account.id } });
      // Update customers
      for (let c = 0; c < customers.length; c++) {
        const customer = customers[c];
        // Update customer
        await customer.updateAttributes({ billableRate: (customer.billableRate || 0) * account.totalHourInDay });
      }


      const users = await models.User.findAll({ where: { accountId: account.id } });

      // Update users
      for (let u = 0; u < users.length; u++) {
        const user = users[u];

        // Update user
        await user.updateAttributes({ billableRate: (user.billableRate || 0) * account.totalHourInDay });
      }

      const projects = await models.Project.findAll({ where: { accountId: account.id } });

      // Update projects and projects categories
      for (let p = 0; p < projects.length; p++) {
        const project = projects[p];
        // Get project categories

        // Update model
        await project.updateAttributes({ billableValue: ((project.billableValue || 0) * account.totalHourInDay) });

        const categories = await models.ProjectCategory.findAll({ where: { projectId: project.id } });
        for (let ca = 0; ca < categories.length; ca++) {
          const category = categories[ca];

          // Update model
          await category.updateAttributes({ billableValue: ((category.billableValue || 0) * account.totalHourInDay) });
        }
      }
    }
  },

  down: async () => {
    // Get all accounts
    const accounts = await models.Account.findAll();

    for (let a = 0; a < accounts.length; a++) {
      const account = accounts[a];
      // Get all datas
      // Update account
      await account.updateAttributes({ billableRate: ((account.billableRate || 0) / account.totalHourInDay) });


      const customers = await models.Customer.findAll({ where: { accountId: account.id } });
      // Update customers
      for (let c = 0; c < customers.length; c++) {
        const customer = customers[c];
        // Update customer
        await customer.updateAttributes({ billableRate: (customer.billableRate || 0) / account.totalHourInDay });
      }


      const users = await models.User.findAll({ where: { accountId: account.id } });

      // Update users
      for (let u = 0; u < users.length; u++) {
        const user = users[u];

        // Update user
        await user.updateAttributes({ billableRate: (user.billableRate || 0) / account.totalHourInDay });
      }

      const projects = await models.Project.findAll({ where: { accountId: account.id } });

      // Update projects and projects categories
      for (let p = 0; p < projects.length; p++) {
        const project = projects[p];
        // Get project categories

        // Update model
        await project.updateAttributes({ billableValue: ((project.billableValue || 0) / account.totalHourInDay) });

        const categories = await models.ProjectCategory.findAll({ where: { projectId: project.id } });
        for (let ca = 0; ca < categories.length; ca++) {
          const category = categories[ca];

          // Update model
          await category.updateAttributes({ billableValue: ((category.billableValue || 0) / account.totalHourInDay) });
        }
      }
    }
  }
};
