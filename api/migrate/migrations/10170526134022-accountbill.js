'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.addColumn(models.AccountBill.tableName, 'frameUpdatedAt', { type: Sequelize.DATE });
    queryInterface.addColumn(models.Account.tableName, 'suspendedAt', { type: Sequelize.DATE });
    queryInterface.addColumn(models.User.tableName, 'acceptedAt', { type: Sequelize.DATE });
  },

  down: function down(queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.removeColumn(models.AccountBill.tableName, 'frameUpdatedAt');
    queryInterface.removeColumn(models.Account.tableName, 'suspendedAt');
    queryInterface.removeColumn(models.User.tableName, 'acceptedAt');
  }
};
