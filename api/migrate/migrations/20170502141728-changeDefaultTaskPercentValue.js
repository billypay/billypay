'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.changeColumn(models.ProjectCategoryTask.tableName, 'percent', { type: Sequelize.INTEGER, defaultValue: 0 });
  },

  down: function down(queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.changeColumn(models.ProjectCategoryTask.tableName, 'percent', { type: Sequelize.INTEGER, defaultValue: 1 });
  }
};
