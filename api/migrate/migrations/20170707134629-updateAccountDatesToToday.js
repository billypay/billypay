'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn(models.Account.tableName, 'updatedAt');
    queryInterface.removeColumn(models.Account.tableName, 'createdAt');

    queryInterface.removeColumn(models.AccountBill.tableName, 'updatedAt');
    queryInterface.removeColumn(models.AccountBill.tableName, 'createdAt');

    queryInterface.addColumn(models.Account.tableName, 'updatedAt', { type: Sequelize.DATE, defaultValue: new Date() });
    queryInterface.addColumn(models.Account.tableName, 'createdAt', { type: Sequelize.DATE, defaultValue: new Date() });

    queryInterface.addColumn(models.AccountBill.tableName, 'updatedAt', { type: Sequelize.DATE, defaultValue: new Date() });
    queryInterface.addColumn(models.AccountBill.tableName, 'createdAt', { type: Sequelize.DATE, defaultValue: new Date() });
  },
  down: async () => {

  },
};
