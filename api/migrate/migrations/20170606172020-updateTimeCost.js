'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: async () => {
    // Get all accounts
    const accounts = await models.Account.findAll();

    for (let a = 0; a < accounts.length; a++) {
      const account = accounts[a];

      // Get all account times
      const times = await models.Time.findAll({ where: { accountId: account.id } });

      for (let t = 0; t < times.length; t++) {
        const time = times[t];
        // Update time cost rate
        await time.updateAttributes({ costRate: time.costRate / account.totalHourInDay });
      }
    }
  },

  down: async () => {
    // Get all accounts
    const accounts = await models.Account.findAll();

    for (let a = 0; a < accounts.length; a++) {
      const account = accounts[a];

      // Get all account times
      const times = await models.Time.findAll({ where: { accountId: account.id } });

      for (let t = 0; t < times.length; t++) {
        const time = times[t];
        // Update time cost rate
        await time.updateAttributes({ costRate: time.costRate * account.totalHourInDay });
      }
    }
  }
};
