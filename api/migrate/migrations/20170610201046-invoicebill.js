'use strict';
const models = require('../../src/modules/graph/models');

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.addColumn(models.AccountBillInvoice.tableName, 'subTotal', { type: Sequelize.FLOAT });
    queryInterface.addColumn(models.AccountBillInvoice.tableName, 'vatPercent', { type: Sequelize.FLOAT });
    queryInterface.addColumn(models.AccountBillInvoice.tableName, 'vatAmount', { type: Sequelize.FLOAT });
  },

  down: function down(queryInterface) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.removeColumn(models.AccountBillInvoice.tableName, 'trial');
    queryInterface.removeColumn(models.AccountBillInvoice.tableName, 'vatPercent');
    queryInterface.removeColumn(models.AccountBillInvoice.tableName, 'vatAmount');
  }
};
