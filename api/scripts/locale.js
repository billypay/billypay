/**
 * Locale configuration
 * This file is private must not preset in git
 * @type {Object}
 */
module.exports = {
  // Express application port
  APPLICATION_PORT: 3000,

  // Environnement executed current application
  ENVIRONNEMENT_APPLICATION: 'development', // (development | production)

  // Database configuration
  // 'mysql'|'mariadb'|'sqlite'|'postgres'|'mssql'
  DATABASE: 'postgres://postgres:root@localhost:5432/billypay',

  DATABASE_TESTING: 'postgres://postgres:root@localhost:5432/billypay_test',

  // Update
  // SENTRY: true,
};
