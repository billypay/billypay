/**
 * Locale configuration
 * This file is private must not preset in git
 * @type {Object}
 */
module.exports = {
  // Express application port
  APPLICATION_PORT: 3000,

  // Environnement executed current application
  ENVIRONNEMENT_APPLICATION: 'development', // (development | production)

  // Database configuration
  // 'mysql'|'mariadb'|'sqlite'|'postgres'|'mssql'
  DATABASE: 'postgres://alexandre:root@localhost:5432/billypay',

  CORE_XSLX_URL: 'http://127.0.0.1:5000',

  // SENTRY: true,

  DISABLED_CACHE: false,

};
