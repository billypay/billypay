# BillyPay API

For working with BillyPay, you must install several packages.
- [Node.js](https://nodejs.org) 7.6
- Postgres >= 9.5
- Redis

##### API

###### 1. Go in api directory and install globals and projects node modules with:
`npm install`

###### 2. Put your locale configuration with:
`cp scripts/locale.js src/config/locale.js`

###### 3. Enjoy
`npm start`
