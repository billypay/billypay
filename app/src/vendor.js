// Loading assets
import '!style!css!less!bootstrap/dist/css/bootstrap.css'
import '!style!css!less!react-select/dist/react-select.css'
import '!style!css!less!react-datepicker/dist/react-datepicker.css'

import './assets/less/fonts.less'
import './assets/less/theme.less'


import './assets/less/responsive.less'
