import React from 'react'
import { graph } from 'shared/graph'

export default class Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      message: 'API is not running :('
    }
  }

  componentWillMount() {
    graph('{ testRoute }').then((res) => {
      if (!!res && !!res.testRoute) {
        this.setState({ message: res.testRoute });
      }
    })
  }

  render() {
    return (
      <div>
        Hello world
        <br />
        {this.state.message}
      </div>
    )
  }
}
