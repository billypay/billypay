# BillyPay App

To start using Billy, make sure you have those tools installed on your computer :
- [Node.js](https://nodejs.org)

##### App

###### 1. Go in api directory and install node modules with:
`npm install`

###### 2. Put your locale configuration with:
`cp scripts/locale.js src/app/locale.js`

###### 3. Enjoy
`npm start`
